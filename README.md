# diablo2ahk

Quality of Life Diablo 2 AutoHotKey Script

Just a simple script replacing the numpad(numlocked) with following functions:
- 1 to 8 for /playersx with the respective number
- Control a simple run counter -> increase("+"), decrease("-"), current value ("enter"), reset("0")
- "/" for /lock
- * for /nopickup