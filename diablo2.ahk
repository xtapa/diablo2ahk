;List of keys under 'https://www.autohotkey.com/docs/KeyList.htm'
;

SendToChat(output) {
    Send, {Return}
    Sleep, 250
    Send, %output%
    Send, {Return}
    return
}

CounterMessage(cnt) {
    m = run counter: %cnt%
    return m
}

NumpadAdd::
cnt++
SendToChat(CounterMessage(cnt))
return

NumpadSub::
cnt--
SendToChat(CounterMessage(cnt))
return

Numpad0::
cnt = 0
SendToChat("reset counter")
return

NumpadEnter::
SendToChat(CounterMessage(cnt))
return

NumpadMult::
SendToChat("/nopickup")
return

NumpadDiv::
SendToChat("/lock")
return

Numpad1::
SendToChat("/players1")
return

Numpad2::
SendToChat("/players2")
return

Numpad3::
SendToChat("/players3")
return

Numpad4::
SendToChat("/players4")
return

Numpad5::
SendToChat("/players5")
return

Numpad6::
SendToChat("/players6")
return

Numpad7::
SendToChat("/players7")
return

Numpad8::
SendToChat("/players8")
return
